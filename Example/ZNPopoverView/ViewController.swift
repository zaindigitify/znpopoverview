//
//  ViewController.swift
//  ZNPopoverView
//
//  Created by zaindigitify on 01/15/2019.
//  Copyright (c) 2019 zaindigitify. All rights reserved.
//

import UIKit
import ZNPopOverView

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func showPreview(_ sender: UIButton) {
        let width: CGFloat = 100
        ZNPopOverView.show(inView: view, at: sender.center, withTitle: "my name is khan and i am not a terrorist", withOptions: [ZNPopOverView.Key.maximumWidth: width, ZNPopOverView.Key.showsCancelButton: true])
    }

}

