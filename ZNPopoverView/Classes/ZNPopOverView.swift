//
//  ZNPopOverView.swift
//  ZNPopOverView
//
//  Created by Zain on 15/11/2018.
//  Copyright © 2018 Zain. All rights reserved.
//

import Foundation
import UIKit

public class ZNPopOverView: UIView {
    
    private var startPoint: CGPoint
    private var presentingViewRect: CGRect
    private var size: CGSize
    private var pointerSize: CGSize = CGSize(width: 20, height: 10)
    private var offset: CGFloat = 15.0
    private var radius: CGFloat = 7.0
    private var direction: ZNPopOverView.PointerDirection = .up
    private var title: String?
    private var cancelButtonIcon: UIImage? = UIImage(named: "znpopovercrossicon")
    private var removesOnTouch: Bool = true
    private var maxWidth:CGFloat = 150
    private var minWidth:CGFloat = 80
    private var maxHeight:CGFloat = 200
    private var font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
    private var animating: Bool = false
    private var options:[Key: Any]?
    private var showsCancelButton: Bool = false
    private var bgColor: UIColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.6)
    private var textColor: UIColor = UIColor.white
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public static func show(inView view: UIView, at origin: CGPoint, withTitle title: String, withOptions options: [Key:Any]? = nil) {
        let popOver = ZNPopOverView(in: view.bounds, startPoint: origin, title: title, withOptions: options)
        view.addSubview(popOver)
        popOver.animate()
    }
    
    private init(size: CGSize = CGSize.zero, in viewRect: CGRect, startPoint: CGPoint, title: String, withOptions options: [Key:Any]? = nil) {
        
        if options?[Key.maximumWidth] as? CGFloat == nil {
            maxWidth = viewRect.size.width-30
        } else {
           maxWidth = options?[Key.maximumWidth] as! CGFloat
        }
        if options?[Key.maximumHeight] as? CGFloat == nil {
            maxHeight = viewRect.size.width-30
        } else {
            maxHeight = options?[Key.maximumHeight] as! CGFloat
        }
        
        presentingViewRect = viewRect
        self.title = title
        self.startPoint = startPoint
        self.size = size
        
        if self.size.width == 0 && size.height == 0 {
            let titleWidth = title.width(withConstrainedHeight: CGFloat.greatestFiniteMagnitude, font: font)
            if titleWidth > maxWidth-offset {
                self.size.width = maxWidth
            } else if (titleWidth < minWidth) {
                self.size.width = minWidth
            } else {
                self.size.width = titleWidth+offset
            }
            let titleHeight = title.height(withConstrainedWidth: self.size.width-offset, font: font)
            self.size.height = titleHeight+offset
            if titleHeight > maxHeight-offset {
                self.size.height = maxHeight
            }
        }
        
        super.init(frame: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height+pointerSize.height))
        setOptions(options)
        pointerDirection()
        backgroundColor = UIColor.clear
        self.clipsToBounds = false
        addTitle()
        
        if showsCancelButton {
            addCancelButton()
        }
    }
    
    private func setOptions(_ options: [Key:Any]?) {
        self.options = options
        maxHeight = options?[Key.maximumHeight] as? CGFloat ?? maxHeight
        maxWidth = options?[Key.maximumWidth] as? CGFloat ?? maxWidth
        font = options?[Key.font] as? UIFont ?? font
        cancelButtonIcon = options?[Key.cancelButtonIcon] as? UIImage ?? cancelButtonIcon
        removesOnTouch = options?[Key.removesOnTouch] as? Bool ?? removesOnTouch
        showsCancelButton = options?[Key.showsCancelButton] as? Bool ?? showsCancelButton
        bgColor = options?[Key.backgroundColor] as? UIColor ?? bgColor
        textColor = options?[Key.textColor] as? UIColor ?? textColor
        radius = options?[Key.cornorRadius] as? CGFloat ?? radius
    }
    
    
    private func pointerDirection() {
        if startPoint.y + size.height + pointerSize.height > presentingViewRect.size.height {
            direction = .down
        }
    }
    
    private func viewFrameAndPointerCenter() -> (CGRect, CGPoint, CGRect) {
        var viewFrame = CGRect.zero
        var pointerCenter = CGPoint.zero
        var popupFrame = CGRect.zero
        
        viewFrame.size.height = size.height+pointerSize.height
        viewFrame.size.width = size.width
        popupFrame.origin.x = 0
        popupFrame.size.width = size.width
        popupFrame.size.height = size.height
        pointerCenter.x = size.width/2
        
        viewFrame.origin.x = startPoint.x-size.width/2
        
        if startPoint.x-size.width/2 < offset {
            viewFrame.origin.x = offset
        }
        if startPoint.x+size.width/2 > presentingViewRect.size.width-offset {
            viewFrame.origin.x = presentingViewRect.size.width-size.width-offset
        }
        
        pointerCenter.x = startPoint.x-viewFrame.origin.x
        if pointerCenter.x-pointerSize.width/2 < radius {
            pointerCenter.x = radius+pointerSize.width/2
        }
        if pointerCenter.x+pointerSize.width/2 > size.width-radius {
            pointerCenter.x = size.width-radius-pointerSize.width/2
        }
        
        if presentingViewRect.size.width - startPoint.x < offset {
            
            pointerCenter.x = size.width-radius-(pointerSize.width/2.0)
        }
        
        switch direction {
        case .up:
            viewFrame.origin.y = startPoint.y
            pointerCenter.y = pointerSize.height/2.0
            popupFrame.origin.y = pointerSize.height
            pointerCenter.y += 1.0
            break
        case .down:
            viewFrame.origin.y = startPoint.y-pointerSize.height-size.height
            pointerCenter.y = size.height+(pointerSize.height/2.0)
            popupFrame.origin.y = 0
            pointerCenter.y -= 1.0
            break
        }
        
        return (viewFrame, pointerCenter, popupFrame)
    }
    
    
    override public func draw(_ rect: CGRect) {
        let (viewFrame, pointerCenter, popupFrame) = viewFrameAndPointerCenter()
        
        self.frame = viewFrame
        
        let ctx : CGContext = UIGraphicsGetCurrentContext()!
        
        ctx.beginPath()
        
        if direction == .up {
            ctx.move(to: CGPoint(x: pointerCenter.x-pointerSize.width/2, y: popupFrame.origin.y))
            ctx.addLine(to: CGPoint(x: pointerCenter.x, y: pointerCenter.y-pointerSize.height/2))
            ctx.addLine(to: CGPoint(x: pointerCenter.x+pointerSize.width/2, y: popupFrame.origin.y))
            ctx.addPath(UIBezierPath(roundedRect: popupFrame, cornerRadius: radius).cgPath)
        } else {
            ctx.move(to: CGPoint(x: pointerCenter.x-pointerSize.width/2, y: popupFrame.origin.y+popupFrame.size.height))
            ctx.addLine(to: CGPoint(x: pointerCenter.x, y: pointerCenter.y+pointerSize.height/2))
            ctx.addLine(to: CGPoint(x: pointerCenter.x+pointerSize.width/2, y: popupFrame.origin.y+popupFrame.size.height))
            ctx.addPath(UIBezierPath(roundedRect: popupFrame, cornerRadius: radius).cgPath)
        }
        
        ctx.closePath()
        ctx.setFillColor(bgColor.cgColor)
        ctx.fillPath()
    }
    
    private func addTitle() {
        if title == nil {
            return
        }
        let y: CGFloat
        if direction == .up {
            y = pointerSize.height+offset/2
        } else {
            y = offset/2
        }
        
        let label = UILabel(frame: CGRect(x: offset/2, y: y, width: frame.size.width-offset, height: self.size.height-offset))
        label.text = title
        label.font = font
        label.textAlignment = .center
        label.textColor = textColor
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        addSubview(label)
    }
    
    private func addCancelButton() {
        let yOffset: CGFloat
        if direction == .up {
            yOffset = pointerSize.height
        } else {
            yOffset = 0
        }

        let buttonWidth:CGFloat = 40
        let cancel = UIButton(frame: CGRect(x: frame.size.width-4-(buttonWidth/2), y: yOffset+3-(buttonWidth/2), width: buttonWidth, height: buttonWidth))
        cancel.setImage(cancelButtonIcon, for: .normal)
        cancel.layer.cornerRadius = buttonWidth/2
        cancel.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        cancel.addTarget(self, action: #selector(cancelAction(_:)), for: .touchUpInside)
        addSubview(cancel)
    }
    
    @objc
    private func cancelAction(_ sender: UIButton) {
        remove()
    }
    
    private func animate() {
        self.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1.0
        }
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !removesOnTouch {
            return
        }
        if animating {
            return
        }
        remove()
    }
    
    private func remove() {
        animating = true
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) {[weak self] (success) in
            self?.animating = false
            self?.removeFromSuperview()
        }
    }
    
    public enum PointerDirection: Int {
        case up = 1
        case down = 2
    }
    
    public enum Key: String {
        case removesOnTouch = "removesOnTouch"
        case font = "font"
        case textColor = "textColor"
        case maximumWidth = "maximumWidth"
        case maximumHeight = "maximumHeight"
        case showsCancelButton = "showsCancelButton"
        case cancelButtonIcon = "cancelButtonIcon"
        case backgroundColor = "backgroundColor"
        case cornorRadius = "cornorRadius"
    }
}

fileprivate extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes:[.font: font], context: nil)
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        return ceil(boundingBox.width)
    }
}
