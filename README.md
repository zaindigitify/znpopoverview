# ZNPopoverView

[![CI Status](https://img.shields.io/travis/zaindigitify/ZNPopoverView.svg?style=flat)](https://travis-ci.org/zaindigitify/ZNPopoverView)
[![Version](https://img.shields.io/cocoapods/v/ZNPopoverView.svg?style=flat)](https://cocoapods.org/pods/ZNPopoverView)
[![License](https://img.shields.io/cocoapods/l/ZNPopoverView.svg?style=flat)](https://cocoapods.org/pods/ZNPopoverView)
[![Platform](https://img.shields.io/cocoapods/p/ZNPopoverView.svg?style=flat)](https://cocoapods.org/pods/ZNPopoverView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ZNPopoverView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ZNPopoverView'
```

## Usage

Import ZNPopOverView where you wan to use

```Swift
import ZNPopOverView
```
Then to show pop over view call:

```Swift
ZNPopOverView.show(inView: view, at: sender.center, withTitle: "[Your title here]", withOptions: [ZNPopOverView.Key.maximumWidth: width, ZNPopOverView.Key.showsCancelButton: true])
```


## Author

zaindigitify, zain@digitify.com

## License

ZNPopoverView is available under the MIT license. See the LICENSE file for more info.
